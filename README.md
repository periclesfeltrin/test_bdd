#Teste BDD


### Teste Web
  Testes para aplicação da [GetNet](https://site.getnet.com.br/)

#### Cenários

```
@Home
Funcionalidade: Busca da home page

	@CT01
	Cenário: Realizar uma busca na home page
		Dado estou na homepage da GetNet
		Quando busco por "superget"
		E encontro o item "Como acesso a minha conta SuperGet?"
		Entao um modal com as informações do item "Como acesso a minha conta SuperGet?" é exibido
```

### Tecnologia
Tecnologias usadas para desenvolvimento:
- [Java](https://www.oracle.com/technetwork/pt/java/index.html)
- [Selenium Webdriver](https://www.selenium.dev/)
- [Cucumber](https://cucumber.io/)
- [Junit](https://junit.org/junit5/)
- [Maven](https://maven.apache.org)
