package com.periclesfeltrin.web.step;

import com.periclesfeltrin.web.page.Home;
import com.periclesfeltrin.web.page.ResultadosBusca;
import com.periclesfeltrin.web.runs.RunCucumber;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;

public class HomePage extends RunCucumber {
    private Home home;
    private ResultadosBusca resultado_busca;

    @Dado("estou na homepage da GetNet")
    public void estouNaHomepageDaGetNet() {
        home = new Home(driver);
    }

    @Quando("busco por {string}")
    public void buscoPor(String texto) {
        resultado_busca = home.buscar("superget");
    }

    @Quando("encontro o item {string}")
    public void encontroOItem(String item) {
        resultado_busca.buscarNoResultado(item);
    }

    @Então("um modal com as informações do item {string} é exibido")
    public void umModalComAsInformacoesEExibido(String item) {
        Assert.assertEquals(resultado_busca.getModal(), item);
    }
}
