package com.periclesfeltrin.web.step;

import com.periclesfeltrin.web.base.Driver;
import com.periclesfeltrin.web.runs.RunCucumber;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hook extends RunCucumber {

    @Before
    public void preCondicao() {
        Driver driver = new Driver();
        driver.getHomePage("https://site.getnet.com.br/");
        this.driver = driver.getDriver();
    }

    @After
    public void posCondicao() {
        driver.quit();
    }
}