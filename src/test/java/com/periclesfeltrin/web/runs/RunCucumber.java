package com.periclesfeltrin.web.runs;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.openqa.selenium.WebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"},
		features = {"src/test/java/com/periclesfeltrin/web/feature"},
		glue = {"com.periclesfeltrin.web.step"})
public class RunCucumber {
	protected static WebDriver driver;
}