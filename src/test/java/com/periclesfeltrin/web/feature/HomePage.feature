#language:pt

@Home
Funcionalidade: Busca da home page

	@CT01
	Cenário: Realizar uma busca na home page
		Dado estou na homepage da GetNet
		Quando busco por "superget"
		E encontro o item "Como acesso a minha conta SuperGet?"
		Entao um modal com as informações do item "Como acesso a minha conta SuperGet?" é exibido