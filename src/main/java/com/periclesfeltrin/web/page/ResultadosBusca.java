package com.periclesfeltrin.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


public class ResultadosBusca extends PageObject {

    public ResultadosBusca(WebDriver driver) {
            super(driver);
        }

    @FindBy(how = How.XPATH, using =  "//div[@class='o-modal is-modal-open']//div[@class='o-modal__title']")
    protected WebElement modal;

    public void buscarNoResultado(String texto){
        List<WebElement> elements  = driver.findElements(By.xpath("//h3[@class=\"c-search-page__result-title\"]"));
        for (WebElement element: elements) {
           if(element.getText().equals(texto)){
               element.click();
               break;
            }
        }
    }

    public String getModal(){
        return wait.until(ExpectedConditions.visibilityOf(modal)).getText();
    }


}
