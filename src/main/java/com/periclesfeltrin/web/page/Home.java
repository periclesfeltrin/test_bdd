package com.periclesfeltrin.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Home extends PageObject {

    public Home(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "search-trigger")
    protected WebElement search_trigger;

    @FindBy(how = How.ID, using = "global-search-input")
    protected WebElement global_search_input;

    public void clickSearchIcon(){
        wait.until(ExpectedConditions.elementToBeClickable(search_trigger)).click();
    }

    public void inputTextSearch(String text){
        wait.until(ExpectedConditions.visibilityOf(global_search_input)).sendKeys(text);
        global_search_input.submit();
    }

    public ResultadosBusca buscar(String texto_busca){
        waitDocumentReady();
        clickSearchIcon();
        inputTextSearch(texto_busca);
        return new ResultadosBusca(driver);
    }
}